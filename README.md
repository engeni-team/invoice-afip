# Engeni AFIP Adapter

[![N|Solid](https://engeni.com/emails/signature/logo.png)](https://engeni.com)

Engeni AFIP Client is a client to connect with Argentinian AFIP

- Create invoices "A", "B", "C"...
- Create Credit Notes

## Installation

Install composer first. Then:

```bash
composer require engeni/invoice-afip
```

Enjoy.
The Engeni Team.
